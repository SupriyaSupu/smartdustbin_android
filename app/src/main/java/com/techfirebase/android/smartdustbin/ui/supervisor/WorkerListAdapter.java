package com.techfirebase.android.smartdustbin.ui.supervisor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techfirebase.android.smartdustbin.R;
import com.techfirebase.android.smartdustbin.domain.Area;

import java.util.List;

public class WorkerListAdapter extends RecyclerView.Adapter<WorkerListAdapter.MyViewHolder> {
  private List<Area> workerList;

  public WorkerListAdapter(List<Area> workerList) {
    this.workerList = workerList;
  }

  public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView dustbinId, area, workerName, mobileNo;

    public MyViewHolder(View rowItemView) {
      super(rowItemView);
      dustbinId = rowItemView.findViewById(R.id.dustbinId);
      area = rowItemView.findViewById(R.id.areaName);
      workerName = rowItemView.findViewById(R.id.workerName);
      mobileNo = rowItemView.findViewById(R.id.mobileNo);
    }
  }

  @Override
  public WorkerListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View ItemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_worker_list, parent, false);
    return new WorkerListAdapter.MyViewHolder(ItemView);
  }

  @Override
  public void onBindViewHolder(WorkerListAdapter.MyViewHolder holder, int position) {
    Area list = workerList.get(position);
    holder.dustbinId.setText(list.getDustbinId());
    holder.area.setText(list.getAreaName());
    holder.workerName.setText(list.getWorkers().get(0).getWorkerName());
    holder.mobileNo.setText(list.getWorkers().get(0).getWorkerMobileNo());
  }

  @Override
  public int getItemCount() {
    return workerList.size();
  }
}
