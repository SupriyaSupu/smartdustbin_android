package com.techfirebase.android.smartdustbin.domain;

public class DustbinDetail {

  private int dustbinDetailId;

  private String dustbinId;

  private String sensorId;

  public DustbinDetail() {}

  public DustbinDetail(int dustbinDetailId, String dustbinId, String sensorId) {
    this.dustbinDetailId = dustbinDetailId;
    this.dustbinId = dustbinId;
    this.sensorId = sensorId;
  }

  public int getDustbinDetailId() {
    return dustbinDetailId;
  }

  public void setDustbinDetailId(int dustbinDetailId) {
    this.dustbinDetailId = dustbinDetailId;
  }

  public String getDustbinId() {
    return dustbinId;
  }

  public void setDustbinId(String dustbinId) {
    this.dustbinId = dustbinId;
  }

  public String getSensorId() {
    return sensorId;
  }

  public void setSensorId(String sensorId) {
    this.sensorId = sensorId;
  }
}
